# Using bash and not sh, cf. http://stackoverflow.com/a/589300/
# GNU Makefile for https://gitlab.com/rainlash/lex-talionis/
# written by Lilian Besson https://gitlab.com/Naereen

# Using bash and not sh, cf. http://stackoverflow.com/a/589300/
SHELL := /usr/bin/env bash -o pipefail

# == TODO ==
# - installation of dependencies
# - print help
# - launching the game
# - launching the level editor
# - generate documentation?
# - launch tests?

# == Possible todo? ==
# - build and upload to PyPI?
#   The game is not Pure Python, and it's not so useful to be able to install it with PyPI

# == Linters ==
# NPROC = `nproc`
NPROC = 4
# NPROC = `getconf _NPROCESSORS_ONLN`

all:	lint lint3
lint:	lint_all lint_Code lint_Utilities lint_Editor
lint3:	lint3_all lint3_Code lint3_Utilities lint3_Editor

lint_all:
	-pylint -j $(NPROC) ./*.py ./*/*.py | tee ./logs/all_pylint_log.txt
lint3_all:
	-pylint --py3k -j $(NPROC) ./*.py ./*/*.py | tee ./logs/all_pylint3_log.txt

lint_Code:
	-pylint -j $(NPROC) ./Code/*.py | tee ./logs/Code_pylint_log.txt
lint3_Code:
	-pylint --py3k -j $(NPROC) ./Code/*.py | tee ./logs/Code_pylint3_log.txt

lint_Utilities:
	-pylint -j $(NPROC) ./Code/*.py | tee ./logs/Utilities_pylint_log.txt
lint3_Utilities:
	-pylint --py3k -j $(NPROC) ./Code/*.py | tee ./logs/Utilities_pylint3_log.txt

lint_Editor:
	-pylint -j $(NPROC) ./Code/*.py | tee ./logs/Editor_pylint_log.txt
lint3_Editor:
	-pylint --py3k -j $(NPROC) ./Code/*.py | tee ./logs/Editor_pylint3_log.txt

# == Installers ==
install3:
	sudo -H pip3 install -U -r requirements.txt

# TODO virtualenv?

# == Stats ==
stats:
	git-complete-stats.sh | tee complete-stats.txt
	git-cal --ascii | tee -a complete-stats.txt
	git wdiff complete-stats.txt

# TODO UML ?
